<?php
    require("animal.php");
    require("frog.php");
    require("ape.php");

    
    $sheep = new Animal("shaun");

    echo "Nama binatang : $sheep->name <br>"; // "shaun"
    echo "Jumlah kaki : $sheep->legs <br>"; // 2
    echo "Apakah masuk kategori binatang berdarah dingin? ";
    var_dump($sheep->cold_blooded); // false
    echo "<br><br>";

    $sungokong = new Ape("kera sakti");
    echo "Nama binatang : $sungokong->name <br>";
    echo "Jumlah kaki : $sungokong->legs <br>";
    echo "Apakah masuk kategori binatang berdarah dingin? ";
    var_dump($sungokong->cold_blooded);
    echo "<br>Suara : ";
    $sungokong->yell(); // "Auooo"
    echo "<br><br>";

    $kodok = new Frog("buduk");
    echo "Nama binatang : $kodok->name <br>";
    echo "Jumlah kaki : $kodok->legs <br>";
    echo "Apakah masuk kategori binatang berdarah dingin? ";
    var_dump($kodok->cold_blooded);
    echo "<br>Suara : ";
    $kodok->jump() ; // "hop hop"


?>
